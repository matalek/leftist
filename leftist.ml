(* Aleksander Matusiak *)
(* Drzewa lewicowe *)

(* Typ złączalnej kolejki priorytetowej *)
(* lewe_poddrzewo, prawe_poddrzewo, wartość, prawa wysokość *)
type 'a queue = 
    Pusty|Node of 'a queue *'a queue *'a * int;;

(* Pusta kolejka priorytetowa *)
let empty = Pusty;;

exception Niemozliwe;;

(* [join q1 q1] zwraca złączenie kolejek [q1] i [q2] *)
let rec join q1 q2 =
    match (q1,q2) with
      (Pusty,a) -> a
     |(a,Pusty) -> a
     |(Node(_,_,p1,_),Node(_,_,p2,_)) -> 
	 let (q1,q2) = if (p1 <= p2) then (q1,q2)
	               else (q2,q1)
	 in match (q1,q2) with
	   (Node(l1,r1,p1,h1),Node(l2,r2,p2,h2)) -> (
	     let q3 = join r1 q2
	     in match (l1,q3) with
	        (Pusty,_) -> Node(q3,Pusty,p1,1)
	       |(_,Pusty) -> raise Niemozliwe
	       |(Node(_,_,_,hl1),Node(_,_,_,hq3)) ->
		    if hl1 > hq3 then Node(l1,q3,p1,hq3+1)
		    else Node(q3,l1,p1,hl1+1) )
	  |_ -> raise Niemozliwe;;
 
(* [add e q] zwraca kolejkę powstałą z dołączenia elementu [e] 
    do kolejki [q] *)    
let add e q = join (Node(Pusty,Pusty,e,1)) q;;

(* Wyjątek podnoszony przez [delete_min] gdy kolejka jest pusta *)
exception Empty;;

(* Dla niepustej kolejki [q], [delete_min q] zwraca parę [(e,q')] gdzie [e]
    jest elementem minimalnym kolejki [q] a [q'] to [q] bez elementu [e].
    Jeśli [q] jest puste podnosi wyjątek [Empty]. *)
let delete_min q =
  match q with
    Pusty -> raise Empty
   |Node(l,r,p,h) -> (p,join l r);;

(* Zwraca [true] jeśli dana kolejka jest pusta. W przeciwnym razie [false] *)
let is_empty q = 
  q = Pusty;;

(* Testowanie *)
let rec dodaj_masowo l q =
  match l with 
    []->q
   |h::t -> dodaj_masowo t (add h q);;

